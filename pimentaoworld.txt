Let's make a game!

	name:Pimentao World
	by:Norbeto
	desc:Clica no <b>pimentão</b> para fazer mais pimentão.
	created:16/02/2022
	updated:16/02/2022
	version:1.0a


Settings
	background:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/backgroud.png
	building cost increase:110%
	building cost refund:25%

Layout
	use default

Buttons
	*grandePimentao
		name:Pimentão
		desc:Clica no pimentão para ter mais pimentão.
		on click:anim icon wobble
		on click:yield 1 pimentao
		on click:if (have cor and chance(5%)) yield 1 redPimentao
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao_smol.png
		no text
		class:bigButton
		icon class:shadowed
		tooltip origin:bottom
		tooltip class:red

Resources
	*pimentao
		name:Pimentão
		desc:Tens poucos pimentão, vai comprar cenas com estes para teres mais.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		class:noBackground
		show earned
	*redPimentao
		name:Pimentão Vermelho
		desc:Podes usar estes pimentão para comprar cenas macabras.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		class:noBackground
		hidden when 0
	*yPimentao
		name:Pimentão Amarelo
		desc:Estes pimentão são muito raros, então gasta-os com consciência.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		class:noBackground
		hidden when 0


Shinies
	*shinyPimentao
		movement:onBottom moveTop
		frequency:300
		frequency variation: 300
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		class:bigButton
		on click:yield 1 yPimentao

	*reddyPimentao
		movement:onLeft moveRight
		frequency:120
		frequency variation: 30
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		class:bigButton
		on click:yield 1 redPimentao

Buildings
	*TEMPLATE
		on click:anim glow
	
	*polvora
		name:Pólvora
		desc:Enche os pimentão de pólvora para criar mais piemntão.<//><b>Efeitos:</b><.>Mais 1 pimentão/click.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:50 pimentao
		passive:increase pimentao yield of grandePimentao by 1
		passive:if (have enxofre) increase pimentao yield of grandePimentao by 1
		passive:if (have nitroglicerina) increase pimentao yield of grandePimentao by 1
		passive:if (have uranio) increase pimentao yield of grandePimentao by 2
		passive:if (have hidrogenio) increase pimentao yield of grandePimentao by 3
		unlocked

	*norbeto|norbetos
		name:Norbeto|Norbetos
		desc:Estes Norbetos podem fazer pimentão por ti.<//><b>Efeitos:</b><.>Fazem um pimentão por segundo.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:150 pimentao
		on tick:yield 1 pimentao
		req:75 pimentao:earned

	*escravo|escravos
		name:Escravo|Escravos
		desc:Compra escravos para trabalharem por ti.<//><b>Efeitos:</b><.>Fazem 0.001 pimentão por segundo.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:1 pimentao
		cost refund:70%
		cost increase:102%
		on tick:yield 0.001 pimentao
		req:1000 pimentao:earned

Upgrades
	*TEMPLATE
		on click:anim glow

	//cor upgrades
	
	*cor
		name:Tinta Vermelha
		desc:Pinta alguns dos pimentão de vermelho.<//><b>Efeitos:</b><.>5% de chances/click de ganhar um pimentão vermelho.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:600 pimentao
		req:500 pimentao:earned

	//building upgrades
	
	*chicote
		name:Chicote
		desc:Dá um pouco de incentivo aos escravos.<//><b>Efeitos:</b><.>Escravos fazem 2 vezes mais pimentão.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:2000 pimentao
		passive:multiply yield of escravo by 2
		req:escravo>=5000

	*p2b
		name:Pimentão Pimentão Beam
		desc:Dá o poder do P2B aos Norbeto para eles fazerem mais pimentão.<//><b>Efeitos:</b><.>Norbetos fazem 3 vezes mais pimentão.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:5000 pimentao
		passive:multiply yield of norbeto by 3
		req:norbeto>=75

	*enxofre
		name:Enxofre
		desc:Mistura algum enxofre à pólvora metida nos pimentões para melhorar o resultado.<//><b>Efeitos:</b><.>+1 pimentão/polvora.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:1000
		req:polvora>=50

	*nitroglicerina
		name:Nitroglicerina
		desc:Também podes adicionar um pouco de nitroglicerina à mistura.<//><b>Efeitos:</b><.>+1 pimentão/polvora.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:15000
		req:polvora>=500

	*uranio
		name:Urânio
		desc:Usa isto para conseguir mais pimentão. Mas tem cuidado, urânio é um pouco explosivo.<//><b>Efeitos:</b><.>+2 pimentão/polvora.
		cost:500000
		req:polvora>=50000

	*hidrogenio
		name:Hidrogénio
		desc:Por mais estranho pareça, hidrogénio é mais perigoso do que urânio, então tem cuidado.<//><b>Efeitos:</b><.>+3 pimentão/polvora.
		icon:https://gitlab.com/betocopia007/pimentaoworld/-/raw/main/pimentao.png
		cost:5000000
		req:polvora>=5000000